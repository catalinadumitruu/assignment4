import React, { Component } from 'react'
import RobotStore from '../stores/RobotStore'

class RobotForm extends Component {

    constructor(props){
        super(props)

        this.state = {
            type : '',
            name : '',
            mass : ''
        }

        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }

        this.add = () => {
            this.props.onAdd({
                type : this.state.type,
                name : this.state.name,
                mass : this.state.mass
            })
        }
    }
  render() {
  	let {item} = this.props
    return (
      <div>
        <form>
  		<input id="name" type="text" placeholder="Robot type" name="type" onChange={this.handleChange} />
        <input id="type" type="text" placeholder="Robot name" name="name" onChange={this.handleChange}/>
        <input id="mass" type="text" placeholder="Robot mass" name="mass" onChange={this.handleChange} />
        <input id="" type="button" value="add" onClick={this.add}/>
        </form>
      </div>
    )
  }
}

export default RobotForm
