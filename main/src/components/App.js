import React, { Component } from 'react'
import RobotList from './RobotList'

class App extends Component {

constructor(props){
  super(props)
}

  render() {
    return (
      <div>
      	A list of robots
      	<RobotList />
      </div>
    )
  }
}

export default App
